module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      animation: {
        fadeout: 'fadeOut 0.5s ease-in-out',
        fadein: 'fadeIn 0.5s ease-in-out'
      },
      keyframes: theme => ({
        fadeOut: {
          '0%': { opacity: '100%' },
          '100%': { 
            opacity: '0%',
           },
        },
        fadeIn: {
          '0%': { 
            opacity: '0%',
         },
          '100%': { 
            opacity: '100%',
           },
        }
      }),
    }
  },
  plugins: [],
}
