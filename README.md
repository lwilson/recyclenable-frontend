# Recyclenaable

Created by Leo Wilson and Zach Mehall for the GWC 2022 Hackathon.

## Installation

To install required dependencies run:
### `npm install`

## Usage
### Development
To start the app in development mode, run:
### `npm start`
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### Production
To create and run a production build, run:
### `npm serve`
Built code will be stored in the `build` directory

### Electron
To start the development electron app, run:
### `npm electron`

Electron can be converted to a binary with the `electron-builder` package, by running:
### `npx electron-builder`
Installers/binaries will be created in `/dist`