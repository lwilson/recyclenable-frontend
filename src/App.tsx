import React from 'react';
import { Api, Item } from './Api';
import { MultipleChoice } from './MultipleChoice';

interface AppState {
  visible: VisibleElement
  loc: string
  dirty: boolean
  msg: string
  recyclable: boolean
  material: string | null
  remaining: Array<Item>
  name: string
}

enum VisibleElement {
  Start, Dirty, Logo, Material, Loading, End, City, Multi
}

class App extends React.Component<{}, AppState> {

  constructor() {
    super({})

    this.state = {
      visible: VisibleElement.Start,
      loc: '',
      dirty: false,
      msg: '',
      recyclable: false,
      material: '',
      remaining: [],
      name: ''
    }
    this.startButton = this.startButton.bind(this)
    this.cityButton = this.cityButton.bind(this)
    this.dirtyButton = this.dirtyButton.bind(this)
    this.materialButton = this.materialButton.bind(this)
    this.startOverButton = this.startOverButton.bind(this)
    this.multiButton = this.multiButton.bind(this)
  }

  hasNoUnrecyclableOptions(items: Array<Item>): boolean {
    let booleans: Array<boolean> = items.map(item => item.recyclable)
    if(booleans.includes(false)) {
      return false
    }
    return true
  }

  hasNoRecyclableOptions(items: Array<Item>): boolean {
    let booleans: Array<boolean> = items.map(item => item.recyclable)
    if(booleans.includes(true)) {
      return false
    }
    return true
  }

  startButton(event: React.MouseEvent<HTMLButtonElement>) {
    this.setState({visible: VisibleElement.City})
    setTimeout(() => {
      document.getElementById("start")?.style.setProperty("display", "none")
      document.getElementById("city")?.style.setProperty("display", "flex")
    }, 400) 
  }

  cityButton(question: string, answer: string) {
    this.setState({
      visible: VisibleElement.Dirty,
      loc: answer.toLowerCase()
    })
    setTimeout(() => {
      document.getElementById("city")?.style.setProperty("display", "none")
      document.getElementById("dirty")?.style.setProperty("display", "flex")
    }, 400) 
  }

  dirtyButton(question: string, answer: string) {
    this.setState({
      visible: VisibleElement.Loading,
      dirty: answer.toLowerCase() === "yes"
    })
    setTimeout(() => {
      document.getElementById("dirty")?.style.setProperty("display", "none")
      document.getElementById("loading")?.style.setProperty("display", "flex")
    }, 400)
    Api.getItems({
      loc: this.state.loc,
      dirty: answer.toLowerCase() === "yes"
    }).then(res => {
      if(res.data.length > 1) {
        this.setState({visible: VisibleElement.Material})
        setTimeout(() => {
          document.getElementById("loading")?.style.setProperty("display", "none")
          document.getElementById("material")?.style.setProperty("display", "flex")
        }, 400) 
      } else {
        this.setState({
          visible: VisibleElement.End,
          recyclable: res.data[0].recyclable,
          msg: res.data[0].msg
        })
        setTimeout(() => {
          document.getElementById("loading")?.style.setProperty("display", "none")
          document.getElementById("end")?.style.setProperty("display", "flex")
        }, 400) 
      }
    })
  }

  materialButton(question: string, answer: string) {
    let material: string | null = answer.toLowerCase() === "other" ? null : answer.toLowerCase()
    if(material === null) {
      this.setState({
        visible: VisibleElement.End,
        recyclable: false,
        msg: "We don't have data on this item, so it probably can't be recycled."
      })
      setTimeout(() => {
        document.getElementById("material")?.style.setProperty("display", "none")
        document.getElementById("end")?.style.setProperty("display", "flex")
      }, 400) 
    } else {
      this.setState({
        visible: VisibleElement.Loading,
        material: material
      })
      setTimeout(() => {
        document.getElementById("material")?.style.setProperty("display", "none")
        document.getElementById("loading")?.style.setProperty("display", "flex")
      }, 400) 
      Api.getItems({
        loc: this.state.loc,
        dirty: this.state.dirty,
        material: material
      }).then(res => {
        if(res.data.length > 1) {
          this.setState({
            visible: VisibleElement.Multi,
            remaining: res.data
          })
          setTimeout(() => {
            document.getElementById("loading")?.style.setProperty("display", "none")
            document.getElementById("multi")?.style.setProperty("display", "flex")
          }, 400) 
        } else {
          this.setState({
            visible: VisibleElement.End,
            recyclable: res.data[0].recyclable,
            msg: res.data[0].msg
          })
          setTimeout(() => {
            document.getElementById("loading")?.style.setProperty("display", "none")
            document.getElementById("end")?.style.setProperty("display", "flex")
          }, 400) 
        }
      })
    }
  }

  multiButton(question: string, answer: string) {
    if(answer.toLowerCase() === "other") {
      this.setState({
        visible: VisibleElement.End,
        recyclable: false,
        msg: "We don't have data on this item, so it probably can't be recycled."
      })
      setTimeout(() => {
        document.getElementById("multi")?.style.setProperty("display", "none")
        document.getElementById("end")?.style.setProperty("display", "flex")
      }, 400) 
    } else {
      this.setState({
        visible: VisibleElement.Loading,
        name: answer
      })
      setTimeout(() => {
        document.getElementById("multi")?.style.setProperty("display", "none")
        document.getElementById("loading")?.style.setProperty("display", "flex")
      }, 400)
      Api.getItems({
        loc: this.state.loc,
        dirty: this.state.dirty,
        material: this.state.material,
        name: answer
      }).then(res => {
        this.setState({
          visible: VisibleElement.End,
          recyclable: res.data[0].recyclable,
          msg: res.data[0].msg
        })
        setTimeout(() => {
          document.getElementById("loading")?.style.setProperty("display", "none")
          document.getElementById("end")?.style.setProperty("display", "flex")
        }, 400)
      })
    }
  }

  startOverButton(event: React.MouseEvent<HTMLButtonElement>) {
    this.setState({
      visible: VisibleElement.Start,
    })
    setTimeout(() => {
      this.setState({
        loc: '',
        dirty: false,
        msg: '',
        recyclable: false,
        material: '',
        remaining: []
      })
      document.getElementById("end")?.style.setProperty("display", "none")
      document.getElementById("start")?.style.setProperty("display", "flex")
    }, 400)
  }

  render() {
    return (
      <div className="font-sans font-semibold bg-slate-200 flex flex-grow h-screen flex-col items-center justify-center">
        <div id='center-box' className="lg:w-1/2 w-2/3 h-2/3 bg-slate-300 items-center flex-col flex rounded-2xl break-words pt-20">
          <div className="lg:text-5xl text-4xl">Recyclenable</div>
          <br />
          <div id="loading" style={{display: "none"}} className={`flex flex-col items-center justify-center ${this.state.visible !== VisibleElement.Loading ? `animate-fadeout` : `animate-fadein`}`}>
            <br />
            <div className="flex items-center justify-center space-x-2 animate-pulse">
                <div className="w-8 h-8 bg-slate-700 rounded-full"></div>
                <div className="w-8 h-8 bg-slate-700 rounded-full"></div>
                <div className="w-8 h-8 bg-slate-700 rounded-full"></div>
            </div>
          </div>
          <div id="start" className={`flex flex-col items-center justify-center ${this.state.visible !== VisibleElement.Start ? `animate-fadeout` : `animate-fadein`}`}>
            <div className={`lg:text-3xl text-xl`}>
              Find if your item is recyclable
            </div>
            <br />
            <button onClick={this.startButton} className="rounded-lg bg-slate-200 cursor-pointer transition hover:bg-slate-100 text-xl pr-10 pt-4 pb-4 pl-10">Begin</button>
          </div>
          <div id="city" style={{display: "none"}} className={`flex flex-col items-center justify-center ${this.state.visible !== VisibleElement.City ? `animate-fadeout` : `animate-fadein`}`}>
            <MultipleChoice question="Which city are you in?" options={["Tempe"]} onClick={this.cityButton} />
          </div>
          <div id="dirty" style={{display: "none"}} className={`flex flex-col items-center justify-center ${this.state.visible !== VisibleElement.Dirty ? `animate-fadeout` : `animate-fadein`}`}>
            <MultipleChoice question="Does the item have food residue on it?" options={["Yes", "No"]} onClick={this.dirtyButton} />
          </div>
          <div id="material" style={{display: "none"}} className={`flex flex-col items-center justify-center ${this.state.visible !== VisibleElement.Material ? `animate-fadeout` : `animate-fadein`}`}>
            <MultipleChoice question="What material is the item?" options={["Paper", "Plastic", "Cardboard", "Aluminum", "Glass", "Styrofoam", "Other"]} onClick={this.materialButton} />
          </div>
          <div id="end" style={{display: "none"}} className={`flex flex-col items-center justify-center ${this.state.visible !== VisibleElement.End ? `animate-fadeout` : `animate-fadein`}`}>
            <div className='text-2xl'>Your item is {this.state.recyclable ? "" : "not "}recyclable</div>
            <br />
            <div className='flex flex-col items-center justify-center text-center mr-4 ml-4'>{this.state.msg}</div>
            <br />
            <button onClick={this.startOverButton} className="rounded-lg bg-slate-200 cursor-pointer transition hover:bg-slate-100 text-xl pr-10 pt-4 pb-4 pl-10">Start Over</button>
          </div>
          <div id="multi" style={{display: "none"}} className={`flex flex-col items-center justify-center ${this.state.visible !== VisibleElement.Multi ? `animate-fadeout` : `animate-fadein`}`}>
            <MultipleChoice question="Choose your item" options={this.state.remaining.map(item => item.props.name).concat("Other") as Array<string>} onClick={this.multiButton} />
          </div>
        </div>
      </div>
    )
  }
}

export default App;
