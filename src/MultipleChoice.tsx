import React from "react";

interface MultipleChoiceProps {
    question: string
    options: Array<string>
    onClick: Function
}

export class MultipleChoice extends React.Component<MultipleChoiceProps> {

    constructor(props: MultipleChoiceProps) {
        super(props)

        this.onClick = this.onClick.bind(this)
    }

    onClick(event: React.MouseEvent<HTMLButtonElement>) {
        this.props.onClick(this.props.question, event.currentTarget.value)
    }

    render() {
        return (
            <div className="flex items-center flex-col">
                <div className="flex m-2 text-xl text-center">{this.props.question}</div>
                <div className="flex m-2 items-center justify-center flex-wrap">
                    {this.props.options.map(option => (
                        <button type="button" value={option} onClick={this.onClick} className={`m-2 p-5 rounded-md bg-slate-200 cursor-pointer transition hover:bg-slate-100`}>{option}</button>
                    ))}
                </div>
            </div>
        )
    }
}