import axios, { AxiosResponse } from 'axios'

interface Query {
  loc: string;
  name?: string;
  dirty?: boolean;
  logo?: string;
  material?: string | null;
}

export interface Item {
  recyclable: boolean;
  props: Query;
  msg?: string;
  products?: string;
  imgURL?: string;
  imgCredit?: string;
}

  
export class Api {

    static baseURL: string = "https://recyclenable.lwilson.dev/api"

    static async getItems(query: Query): Promise<AxiosResponse<any, any>> {
        return await axios.post(`${Api.baseURL}/getitems`, query, {
          headers: {
            "Access-Control-Allow-Headers": "Content-Type"
          }
        })
    }
}